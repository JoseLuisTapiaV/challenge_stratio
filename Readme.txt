########
Entradas:
########
El sistema está formado por los siguiente elementos de entrada:
- Fichero excel Eventos_entrada.xlsx el cual contiene los eventos que van a ser procesado. Los campos que lo componen son:
	- EVENTO: Codigo de evento:
		- 'A'= Alta de un nuevo mafioso
		- 'E'= Encarcelación de un mafioso. 
		- 'S'= Sacar de la carcel a un mafioso
	- NOMBRE: Nombre del mafioso
	- ID_MAFIOSO: Identificador numérico del mafioso
	- DEPENDE: Identificador del mafioso del que depende
	- FECHA_ALTA: Fecha de alta en la organización 
	
- Fichero excel Mafiosos.xls. Datos iniciales de carga con los mafiosos existentes antes de empezar a procesar el fichero de eventos. Este fichero es similar al anterior con la salvedad que no contiene el valor del evento. Los campos son
	- NOMBRE: Nombre del mafioso
	- ID_MAFIOSO: Identificador numérico del mafioso
	- DEPENDE: Identificador del mafioso del que depende
	- FECHA_ALTA: Fecha de alta en la organización 

Hemos divido la entrada en 2 ficheros ya que de este modo las pruebas son más faciles de realizar cargando primeramente el fichero inicial de Mafiosos.xls y luego procesando los eventos del fichero Eventos_entrada.xls

#################################
Estructuras de datos importantes:
#################################
- df_mafia: Estructura de datos (dataframe) que contiene para cada mafioso:
	- NOMBRE Nombre del mafioso
	- ID_MAFIOSO: Identificador numérico del mafioso
	- DEPENDE: Identificador del mafioso del que depende
	- FECHA_ALTA: Fecha de alta en la mafia

- df_prision: Estructura de datos (dataframe) que contiene los datos de los mafiosos encarcelados y de sus subordinados. La estructura es identica a df_mafia

###########
Funciones:
##########
Se han definido las siguientes funciones:
- crea_df_prision: Crea el dataframe df_prision que contiene los datos relevantes de los mafiosos encarcelados y de sus subordinados

- alta_mafioso: Añade un mafioso al df_mafia que contiene las relaciones entre los mafiosos y sus subordinados

- encarcelar_mafioso: Realiza la operativa de mover a un mafioso y sus datos relevante al dataframe df_prision, además de borrarlo de df_mafia.

- salida_prision: Mueve los datos del mafioso y de sus subordinados de df_prision a df_mafia.

- cuenta_subordinados: Cuenta el numero de subordinados y saca una alerta cuando el numero de estos es mayor de 50.


##################
Logica del proceso
##################
La logica del proceso se plantea como una estructura en arbol donde cada nodo es un mafioso y los nodos inferiores son los subordinados
En base a esta estructura la logica del sistema se basa en recorrer el arbol, mover nodos y borrar nodos.
La estructura de arbol se ha representado dentro de la estructura dataframe de Python, siendo cada fila un nodo del arbol y el campos DEPENDE es la conexión con el nodo padre
De este modo, es posible moverse por el dataframe como si fuera un arbol

###############
Procesos Python 
###############
Se adjunta el sistema dentro del fichero Challenge_Final_Version.ipynb para ser abierto desde Jupiter Notebook
Tambien se adjunta el mismo sistema en el fichero Challenge_Final_Version.py para ser abierto desde otra plataforma.



	