
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd


# In[2]:


# Crea el dataframe de eventos que será procesados.
df_datos = pd.read_excel('./Eventos_entrada.xlsx')
print('Fichero de Eventos_entrada.xls')
print(df_datos)
print()


# In[3]:


# Crea dataframe con la carga inicial de los mafiosos de la organizacion. El df_datos puede contener tambien mafiosos
# adicionales que serán dados de alta posteriormente a la carga inicial
df_mafia = pd.read_excel('./Mafiosos.xlsx')
print('Carga inicial de datos Mafiosos.xls')
print(df_mafia)
print()


# In[4]:


##############################################################################################
# Crea el dataframe df_prision donde se insertan los mafiosos encarcelados y sus subordinados
# Este df se utiliza para poder restaurar los valores de mafioso una vez salga de la carcel
##############################################################################################
def crea_df_prision():
    df_prision = pd.DataFrame(data=[], columns=df_mafia.columns)
    return(df_prision)
    


# In[5]:


#######################################
# Añade un nuevo mafioso al df_mafioso
#######################################
def alta_mafioso (df_mafia,nombre_mafioso, id_mafioso, depende, fecha_alta):
    df_mafia.loc[len(df_mafia)]=[nombre_mafioso, id_mafioso, depende, fecha_alta] 
    print('Mafioso dado de Alta.', ' ID_MAFIOSO=', id_mafioso )
    print (df_mafia)
    print ()
    return(df_mafia)


# In[6]:


###########################################################################
# Guardado de los datos del mafiosos a borrar en el df_prision que contiene 
# los datos del id_mafioso y de sus subordinados
###########################################################################
def encarcelar_mafioso(mafioso_borrar, df_mafia, df_prision):
    # Guardamos en el df_prision los valores del mafioso y de sus subordinados.
    # En el futuro cuando el mafioso salga de la carcel se usará este df_prision
    # para restaurar los valores
    df_aux=df_mafia[(df_mafia['ID_MAFIOSO']== mafioso_borrar) | (df_mafia['DEPENDE'] == mafioso_borrar) ]
    df_prision=pd.concat([df_prision,df_aux]).reset_index(drop=True)
    
       
    # Seleccionamos el mentor del mafioso a borrar
    padre_mafioso=int(df_mafia['DEPENDE'][df_mafia['ID_MAFIOSO']==mafioso_borrar])
           
    # Seleccionamos los mafiosos candidatos que son subordinados del mismo mentor del subordinado que queremos borrar
    # excepto el que queremos borra
    df_mafioso_candidatos=df_mafia[(df_mafia['DEPENDE']==padre_mafioso) & (df_mafia['ID_MAFIOSO'] != mafioso_borrar) ]
    convert_dict = {'DEPENDE': int, 'ID_MAFIOSO':int, 'FECHA_ALTA':int} 
    df_mafioso_candidatos = df_mafioso_candidatos.astype(convert_dict) 


    #Comprobamos que hay algun mafioso candidato
    total_candidatos=len(df_mafioso_candidatos)
    
    if total_candidatos > 0:
        ####################################################################################################
        # Funcionalidad cuando hay algún candidato total_candidatos > 0
        # De los mafiosos candidatos seleccionamos el que tiene la fecha mas antigua para que sea el nuevo mentor
        # de los subordinados del mafioso a borrar
        ####################################################################################################
        indice_mafioso_ganador=df_mafioso_candidatos['FECHA_ALTA'].idxmin()
        mafioso_ganador=int(df_mafia.iloc[indice_mafioso_ganador]['ID_MAFIOSO'])    
    
        # Actualizamos todos los mafiosos que dependen del que queremos borrar al nuevo mafioso ganador
        # por ganador entendemos el que tiene fecha_alta máxima (el mas antiguo en la organización)
        df_actualizar_temp = df_mafia[df_mafia['DEPENDE'] == mafioso_borrar].copy()
        df_actualizar_temp['DEPENDE'] = mafioso_ganador
        df_mafia.update(df_actualizar_temp['DEPENDE'])

        #Debido a que la funcion update cambia el tipo a float usamos el siguiente código para volver a int
        convert_dict = {'DEPENDE': int, 'ID_MAFIOSO':int, 'FECHA_ALTA':int} 
        df_mafia = df_mafia.astype(convert_dict) 
    
        # Eliminamos el mafioso a borrar y reemplazamos el dataframe con los datos eliminados
        df_aux=df_mafia.drop(df_mafia[df_mafia['ID_MAFIOSO'] == mafioso_borrar].index).reset_index(drop=True)
        df_mafia=df_aux.copy()
        

    
    else:
        ##########################################################################
        # Funcionalidad cuando no hay candidatos
        ##########################################################################
        
             
        # Seleccionamos los subordinados del mafioso a borrar
        df_mafioso_candidatos=df_mafia[(df_mafia['DEPENDE']==mafioso_borrar)]
        convert_dict = {'DEPENDE': int, 'ID_MAFIOSO':int, 'FECHA_ALTA':int} 
        df_mafioso_candidatos = df_mafioso_candidatos.astype(convert_dict) 
  
         #Guardamos los datos del mafioso ganador
        indice_mafioso_ganador=df_mafioso_candidatos['FECHA_ALTA'].idxmin()    ###<<------
        
        
        mafioso_ganador=int(df_mafia.iloc[indice_mafioso_ganador]['ID_MAFIOSO'])
        nombre_mafioso_ganador=df_mafia.iloc[indice_mafioso_ganador]['NOMBRE']
        fecha_alta_mafioso_ganador=int(df_mafia.iloc[indice_mafioso_ganador]['FECHA_ALTA'])
    
        # La actualizacion la realizamos en 3 fases
        
        # Primero actualizamoS el mafioso a borrar con el subordinado ganador
        # Actualizamos todos los mafiosos que dependen del que queremos borrar al nuevo mafioso ganador
        df_actualizar_temp = df_mafia[df_mafia['ID_MAFIOSO'] == mafioso_borrar].copy()
        df_actualizar_temp['ID_MAFIOSO'] = mafioso_ganador
        df_actualizar_temp['NOMBRE'] = nombre_mafioso_ganador
        df_actualizar_temp['FECHA_ALTA'] = fecha_alta_mafioso_ganador
        df_mafia.update(df_actualizar_temp)

        #Debido a que la funcion update cambia el tipo a float usamos el siguiente código para volver a int
        convert_dict = {'DEPENDE': int, 'ID_MAFIOSO':int, 'FECHA_ALTA':int} 
        df_mafia = df_mafia.astype(convert_dict) 
    
        # Segundo actualizamos los subordinados del mafioso a borrar con el subordinado ganador

        # Actualizamos todos los mafiosos que dependen del que queremos borrar al nuevo mafioso ganador
        df_actualizar_temp = df_mafia[df_mafia['DEPENDE'] == mafioso_borrar].copy()
        df_actualizar_temp['DEPENDE'] = mafioso_ganador
        df_mafia.update(df_actualizar_temp['DEPENDE'])

        #Debido a que la funcion update cambia el tipo a float usamos el siguiente código para volver a int
        convert_dict = {'DEPENDE': int, 'ID_MAFIOSO':int, 'FECHA_ALTA':int} 
        df_mafia = df_mafia.astype(convert_dict) 

        # Tercero borramos el mafioso ganador ,el cual tiene el id y depende igual,
        #ya que se movido al lugar del mafioso a borrar
        df_mafia[df_mafia['ID_MAFIOSO'] == df_mafia['DEPENDE']]
        df_mafia.drop(df_mafia[df_mafia['ID_MAFIOSO'] == df_mafia['DEPENDE']].index).reset_index(drop=True)

    df_mafia.reset_index(drop=True)
    df_prision.reset_index(drop=True)
    
    print('Mafioso Encarcelado.', 'ID_MAFIOSO=', mafioso_borrar )
    print('Padre mafioso=', padre_mafioso )
    print('Total candidatos para ocupar vacante del mafioso encarcelado=', total_candidatos)
    print(df_mafia)
    print()
    print ('Datos de df_prision')
    print (df_prision)
    print()
    
    return(df_mafia, df_prision)


# In[7]:


############################################################################################
# Saca a un mafioso de prision y restaura la estructura de el y sus subordinados en df_mafia
############################################################################################

def salida_prision(salida_prision_mafioso, df_mafia, df_prision):

    # El proceso elimina los subordinados del mafioso porque se van a restaurar los valores de estos desde el df_prision
    # Para ello leemos de df_prision y el valor recuperado lo borra de df_mafia
    df_aux = df_prision[df_prision['DEPENDE'] == salida_prision_mafioso]
    for indice, fila in df_aux.iterrows():
        mafioso_borrar_aux = fila['ID_MAFIOSO']
        indice_borrar_aux=df_mafia[df_mafia['ID_MAFIOSO'] == mafioso_borrar_aux].index
        df_mafia=df_mafia.drop(indice_borrar_aux).reset_index(drop=True)
    
    # Se añade al dataframe df_mafia el mafioso que sale de la carcel y sus subordinados
    df_aux=df_prision[(df_prision['ID_MAFIOSO']== salida_prision_mafioso) | (df_prision['DEPENDE']== salida_prision_mafioso) ]
    df_mafia=pd.concat([df_mafia,df_aux]).reset_index(drop=True)

    # Borramos los datos restaurados sobre df_mafia de df_prision
    # Eliminamos el mafioso a borrar y reemplazamos el dataframe con los datos eliminados
    df_aux=df_prision.drop(df_prision[(df_prision['ID_MAFIOSO'] == salida_prision_mafioso) | (df_prision['DEPENDE'] == salida_prision_mafioso) ].index).reset_index(drop=True)
    df_prision=df_aux.copy()
    
    df_prision.reset_index(drop=True)
    df_mafia.reset_index(drop=True)
    
    print('Mafioso Sale de Prision.', ' ID_MAFIOSO=', salida_prision_mafioso)
    print(df_mafia)
    print()
    print ('Datos de df_prision')
    print (df_prision)
    print()
    
    return(df_mafia, df_prision)


# In[8]:


# Esta función cuenta el numero de subordinados de un mafioso
def cuenta_subordinados (id_mafioso):
    contador_subordinados = 0
    lista_mafia=list()
    #Añadimos el mafioso a buscar en la lista
    lista_mafia.append(id_mafioso)
    
    # Mientras la lista tenga elementos se procesará el bucle
    while len(lista_mafia) > 0:
        # Recorremos df_mafia para buscar los subordinados
        for indice, fila in df_mafia.iterrows():
            # Cuando se encuentra un subordinado se añade al final de la lista 
            # La comparación se realiza con el primero de la lista 
            if fila.DEPENDE == lista_mafia[0] :
                contador_subordinados = contador_subordinados + 1
                lista_mafia.append(fila.ID_MAFIOSO)
                    
        # Borramos el primero de la lista
        lista_mafia.pop(0)
            
    # Escribimos la salida 
    if contador_subordinados > 50:
        print('Mafioso a poner en vigilancia. ', ' Total subordinados=', contador_subordinados)
    else:
        print('Mafioso no peligroso.', ' Total subordinados=', contador_subordinados)


# In[9]:


######################################


# In[10]:


######################################


# In[11]:


######################################


# In[ ]:





# In[12]:


# Creamos el dataframe df_prision
df_prision=crea_df_prision()


# In[13]:


# Los posible eventos que se pueden procesar procedentes de Datos_eventos son:
# - 'A'= Alta de mafioso. Los datos del mafioso se añaden a df_mafia
# - 'E'= Encarcelar mafioso. 
# - 'S'= Sacar de la carcel a un mafioso
for indice, fila in df_datos.iterrows():
        if fila.EVENTO == 'A':
            df_mafia=alta_mafioso(df_mafia,fila.NOMBRE, fila.ID_MAFIOSO, fila.DEPENDE, fila.FECHA_ALTA)
        if fila.EVENTO == 'E':
            df_mafia, df_prision=encarcelar_mafioso(fila.ID_MAFIOSO, df_mafia, df_prision)
        if fila.EVENTO == 'S':
            df_mafia, df_prision=salida_prision(fila.ID_MAFIOSO, df_mafia, df_prision)


# In[14]:


################################################################


# In[15]:


# Contamos los subordinados del mafioso 4 
cuenta_subordinados(4)


# In[16]:


# Contamos los subordinados del mafioso 1
cuenta_subordinados(1)

